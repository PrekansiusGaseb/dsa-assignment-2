import ballerinax/kafka;
import ballerinax/mongodb;
import ballerina/log;
import ballerina/io;

public class Store {
    private mongodb:Client mongoClient;
    private string collection;

    public function init(Configuration configuration) {
        mongodb:ConnectionConfig mongoConfig = {
            host: configuration.host,
            port: configuration.port
        };
        self.mongoClient = checkpanic new (mongoConfig, configuration.database);
        self.collection = configuration.collection;
    }

    public isolated function loginOn(string id, string password) returns boolean|error {
        CustomerQuery customer = {
            id: id,
            password: password
        };

        stream<Customer, error?> result = checkpanic self.mongoClient->find(self.collection, (), customer, 'limit = 1);

        record {|Customer value;|}|error? res = result.next();

        while (res is record {|Customer value;|}) {
            Customer cust = res.value;
            if (id == cust.id && password == cust.password) {
                return true;
            }
        }
        return error("Invalid login credentials");
    }
 
    public isolated function getProducts() returns Product[] {
        stream<Product, error?> products = checkpanic self.mongoClient->find("Product", (), ());
        Product[] product = [];
        record {|Product value;|}|error? res = products.next();
        while (res is record {|Product value;|}) {
            product.push(res.value);
            res = products.next();
        }

        return product;
    }

    
    public function save(OrderConsumerRecord customerOrder) {
        
        map<json> _ = <map<json>>customerOrder.value.toJson();
    }
}

public enum Order_Status {
    PLACED,
    APPROVED,
    DELIVERED
}

public type Product record {|
    string id;
    string name;
    decimal price;
    int quantity;
|};

public type Order record {|
    string id;
    string customerId;
    Product[] products;
    int totalQuantity;
    decimal totalPrice;
    Order_Status status;

|};

public type Customer record {|
    string id;
    string name;
    string email;
    string phone;
    string address;
    string password;
    Order[] orders;
|};
public type CustomerQuery record {|
    string id;
    string password;
|};
 

public type Configuration record {|
    string host;
    int port;
    string database;
    string collection;
|};


public type ProductConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    Product value;
|};
public type OrderConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    Order value;
|};


kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "consumer-group-id-1",
    topics: ["q-daddy-dsa-topic-1"],
    autoCommit: false,
    pollingInterval: 1
};

     Configuration databaseConfig = {
        host: "localhost",
        port: 2181,
        database: "test",
        collection: "Consumer_Prod"
    };

service on new kafka:Listener(kafka:DEFAULT_URL, consumerConfigs) {
    // store;
    private Configuration databaseConfig = {
        host: "localhost",
        port: 2181,
        database: "test",
        collection: "Consumer_Prod"
    };

    // function init() returns error?{
    //    private Store  store = new(databaseConfig);
    // }

     remote function onConsumerRecord(kafka:Caller caller, ProductConsumerRecord[] records) returns error? {
        kafka:Error? results = caller->'commit();
        log:printInfo("Retrieving Products");
        io:println("---------------------------Products---------------------------");
        check from ProductConsumerRecord productRecord in records 
            do {
                io:println("Product ID: ", productRecord.value.id);
                io:println("Product Name: ", productRecord.value.name);
                io:println("Product Price: ", productRecord.value.price);
                io:println("Product Quantity: ", productRecord.value.quantity);
                io:println("------------------------------------------------------------");
            };
    
        if results is kafka:Error {
            log:printInfo("Error occuered");
        }
    }
}
